require "logger"
require "./rhea/compiler"
require "./rhea/lexer"
require "./rhea/parser"

module Rhea
  CONFIG = {
    bits: 64,
    log_level: Logger::DEBUG,
    import_paths: ENV["RHEA_IMPORT_PATH"]?.to_s.split(":").reject(&.empty?),
    out_path: "/tmp/rhea",
  }

  LOG = Logger.new(STDERR)
  LOG.level = CONFIG["log_level"]
  LOG.debug "Starting"
end

case ARGV.first?
when "tokens"
  p Rhea::Lexer.lex File.read ARGV[1]
when "ast"
  p Rhea::Parser.parse Rhea::Lexer.lex File.read ARGV[1]
when "ir"
  Dir.mkdir_p "/tmp/rhea"
  source_path = ARGV[1]
  import_paths = Rhea::CONFIG[:import_paths] + [File.dirname(source_path)]
  source = File.read source_path
  Rhea::Compiler.new(source, import_paths).compile_to(Rhea::CONFIG[:out_path])
end
