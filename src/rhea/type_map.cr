module Rhea
  class TypeMap
    def initialize
      @map = {} of String => Type
    end

    protected def initialize(@map)
    end

    def []=(key : String, val : Type) : Type
      if @map.includes? key
        raise "Redefinition of #{key}"
      end
      @map[key] = val
    end

    def [](key : String) : Type
      @map[key]?.try{ |ty| return ty }
      raise "No such constant: #{key}"
    end

    def [](namespace : Array(String)) : Type
      self[String.build{ |str| namespace.each{ |id| str << '.' << id } }]
    end

    def [](namespace : Array(String), key : String) : Type
      if key.starts_with? "."
        @map[key]?.try{ |ty| return ty }
      else
        root = namespace.dup
        until root.empty?
          full_key = String.build{ |str| root.each{ |id| str << '.' << id }; str << '.' << key }
          @map[full_key]?.try{ |ty| return ty }
          root.pop
        end
        @map[".#{key}"]?.try{ |ty| return ty }
      end

      raise "No such constant: #{key} in #{namespace}"
    end

    def [](namespace : Array(String), key : TypeAST) : Type
      self[namespace, key.name]
    end

    def merge(other)
      ret = TypeMap.new(@map.dup)
      other.each{ |k,v| ret[k] = v }
      return ret
    end

    delegate :each, to: @map
  end
end
