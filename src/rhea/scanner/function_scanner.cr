require "../scanner"
require "../function_signature"

module Rhea
  class Scanner::FunctionScanner < Scanner
    @function_types = {} of String => FunctionSignature

    @type_map : TypeMap
    def initialize(@type_map)
    end

    def scan_init(node : InitAST)
      name = path_str
      type = @path[0..-2].join "."
      arguments = node.arguments.map{ |arg_ast| arg_ast.type.name }
      raise "Duplicate function def: #{name}" if @function_types.includes? name
      LOG.debug{ "Found function: #{name} = #{type}" }
      @function_types[name] = FunctionSignature.new(arguments, type)
      super
    end

    def scan_def(node : DefAST)
      name = path_str
      arguments = node.arguments.map{ |arg_ast| arg_ast.type.name }
      raise "Duplicate function def: #{name}" if @function_types.includes? name
      LOG.debug{ "Found function: #{name} = #{node.type}" }
      @function_types[name] = FunctionSignature.new(arguments, node.type.name)
      super
    end

    def scan_raw_def(node : RawDefAST)
      name = path_str
      arguments = node.arguments.map{ |arg_ast| arg_ast.type.name }
      raise "Duplicate function def: #{name}" if @function_types.includes? name
      LOG.debug{ "Found function: #{name} = #{node.type}" }
      @function_types[name] = FunctionSignature.new(arguments, node.type.name)
      super
    end

    def scan_top_level(node : TopLevelAST)
      super
      @function_types
    end
  end
end
