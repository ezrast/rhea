require "../scanner"

module Rhea
  class Scanner::ConstantScanner < Scanner
    struct Constants
      getter strings = {} of Array(UInt8) => Int32
      getter lambdas = {} of LambdaLitAST => Int32

      def size
        strings.size + lambdas.size
      end
    end

    def initialize
      @constants = Constants.new
    end

    def scan_top_level(node : TopLevelAST)
      super
      return @constants
    end

    @@str_count = 0
    def scan_expr(node : StringLitAST)
      @constants.strings[node.value] ||= @@str_count.tap{ @@str_count += 1 }
    end

    @@lambda_count = 0
    def scan_expr(node : LambdaLitAST)
      @constants.lambdas[node] ||= @@lambda_count.tap{ @@lambda_count += 1 }
      super
    end
  end
end
