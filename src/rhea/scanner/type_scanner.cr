require "../scanner"

module Rhea
  class Scanner::TypeScanner < Scanner
    @type_map = TypeMap.new

    def scan_slice(node : SliceAST)
      @type_map[path_str] = Slice.new(node.name)
    end

    def scan_bi_type(node : BIValueAST)
      LOG.debug{ "Scanning type: #{node}" }
      @type_map[path_str] = BIValue.new(node.ir_name, "." + node.name)
    end

    def scan_top_level(node : TopLevelAST)
      @type_map[".Bool"] = BIBool  # TODO move these to stdlib/_core
      @type_map[".Lambda"] = BILambda
      super
      return @type_map
    end
  end
end
