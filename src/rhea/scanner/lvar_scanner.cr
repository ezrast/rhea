require "../scanner"

module Rhea
  # Maps function paths to lvar type maps
  class Scanner::LVarScanner < Scanner
    @lvar_types = {} of String => Hash(String, Type)

    @function_types : Hash(String, FunctionSignature)
    @type_map : TypeMap
    def initialize(@function_types, @type_map)
    end

    def scan_init(node : InitAST)
      types = @lvar_types[path_str] ||= {} of String => Type

      node.arguments.each do |arg|
        raise "Duplicate lvar def: #{arg.name}" if types.includes? arg.name
        types[arg.name] = @type_map[@path, arg.type]
      end
      super
    end

    def scan_state(node : StateAST)
      types = @lvar_types[path_str] ||= {} of String => Type

      node.arguments.each do |arg|
        raise "Duplicate lvar def: #{arg.name}" if types.includes? arg.name
        types[arg.name] = @type_map[@path, arg.type]
      end
      super
    end

    def scan_def(node : DefAST)
      types = @lvar_types[path_str] ||= {} of String => Type

      node.arguments.each do |arg|
        raise "Duplicate lvar def: #{arg.name}" if types.includes? arg.name
        types[arg.name] = @type_map[@path, arg.type]
      end
      super
    end

    def scan_raw_def(node : RawDefAST)
      types = @lvar_types[path_str] ||= {} of String => Type

      node.arguments.each do |arg|
        raise "Duplicate lvar def: #{arg.name}" if types.includes? arg.name
        types[arg.name] = @type_map[@path, arg.type]
      end
      super
    end

    def scan_expr(node : AssignmentAST)
      super
      raise "Duplicate lvar def: #{node.lvar.name}" if (@lvar_types[path_str] ||= {} of String => Type).includes? node.lvar.name
      @lvar_types[path_str][node.lvar.name] = find_type(node.value)
    end

    def scan_top_level(node : TopLevelAST)
      super
      return @lvar_types
    end

    #########

    def find_type(node : CallAST)
      @type_map[@path, @function_types[node.full_callee].ret_type]
    end

    def find_type(node : MethodAST)
      receiver_type = find_type(node.receiver)
      @type_map[@path, @function_types["#{receiver_type.friendly}.#{node.callee}"].ret_type]
    end

    def find_type(node : AssignmentAST)
      find_type(node.value)
    end

    def find_type(expr : LVarAST)
      @lvar_types[path_str][expr.name]
    end

    def find_type(expr : StringLitAST)
      @type_map[".String"]
    end

    def find_type(expr : IntLitAST)
      @type_map[".Word"]
    end

    def find_type(expr : LambdaLitAST)
      BILambda
    end

    def find_type(expr : RetAST)
      BINoReturn
    end
  end
end
