require "./token"
require "./ast"

module Rhea
  class Parser
    alias TTID = TokenType::TokenTypeID

    def self.parse(tokens : Array(Token))
      new(tokens).parse_top_level
    end

    @tokens : Array(Token)
    @idx : Int32
    private def initialize(@tokens)
      @idx = 0
    end

    def peek(offset = 0)
      @tokens[@idx + offset]?.try &.type_id
    end

    def shift
      @tokens[@idx]?.tap{ @idx += 1 }
    end

    def new_line?(offset = 0)
      return @tokens[@idx + offset].try &.new_line?
    end

    def parse_top_level
      actors = [] of ActorAST
      slices = [] of SliceAST
      imports = [] of ImportAST
      raw_defs = [] of RawDefAST
      bi_types = [] of BIValueAST
      while type_id = peek
        case type_id
        when TTID::KwActor
          actors << parse_actor
        when TTID::KwSlice
          slices << parse_slice
        when TTID::KwImport
          imports << parse_import
        when TTID::KwCoreImport
          imports << parse_core_import
        when TTID::KwRawDef
          raw_defs << parse_raw_def
        when TTID::KwBiValue
          bi_types << parse_bi_value
        else
          expected "Import, Actor, or Slice"
        end
      end

      return TopLevelAST.new(imports, actors, slices, raw_defs, bi_types)
    end

    def parse_import
      expect :KwImport
      path = String.build do |str|
        str << expect(:Identifier).str
        while expect? :Slash
          str << '/'
          str << (expect :Identifier).str
        end
      end

      expect :BracketL
      type_names = [expect(:Constant).str]
      until expect? :BracketR
        expect :Comma
        type_names << expect(:Constant).str
      end

      return ImportAST.new(path, type_names)
    end

    def parse_core_import
      expect :KwCoreImport
      path = String.build do |str|
        str << '_' << expect(:Identifier).str
        while expect? :Slash
          str << '/'
          str << (expect :Identifier).str
        end
      end

      type_names = [""]

      return ImportAST.new(path, type_names)
    end

    def parse_bi_value
      expect :KwBIValue
      id = expect :Constant
      expect :KwIR
      expect :QuoteL
      ir_name = parse_str_body
      expect :QuoteR
      raw_defs = [] of RawDefAST

      until expect? :KwEnd
        raw_defs << parse_raw_def
      end

      return BIValueAST.new(id.str, ir_name, raw_defs)
    end

    def parse_actor
      expect :KwActor
      id = expect :Constant
      states = [] of StateAST
      defs = [] of DefAST
      raw_defs = [] of RawDefAST

      until expect? :KwEnd
        case peek
        when TTID::KwState
          states << parse_state
        when TTID::KwDef
          defs << parse_def
        when TTID::KwRawDef
          raw_defs << parse_raw_def
        else
          expected "State or Def"
        end
      end

      return ActorAST.new(id.str, states, defs, raw_defs)
    end

    def parse_slice
      expect :KwSlice
      id = expect :Constant
      inits = [] of InitAST
      defs = [] of DefAST
      fin = nil

      until expect? :KwEnd
        case peek
        when TTID::KwInit
          inits << parse_init
        when TTID::KwDef
          defs << parse_def
        when TTID::KwFin
          expected "Only one Fin" if fin
          expect :KwFin
          _, _, fin = parse_block(false, false)
        else
          expected "Init, Def or Fin"
        end
      end

      return SliceAST.new(id.str, inits, defs, fin || [] of ExpressionAST)
    end

    def parse_state
      expect :KwState
      id = expect :Identifier
      arguments, _, body = parse_block(true, false)

      return StateAST.new(id.str, arguments, body)
    end

    def parse_init
      expect :KwInit
      id = expect :Identifier
      arguments, _, body = parse_block(true, false)

      return InitAST.new(id.str, arguments, body)
    end

    def parse_def
      expect :KwDef
      id = expect :Identifier
      arguments, ret_type, body = parse_block(true, true)

      return DefAST.new(id.str, arguments, ret_type, body)
    end

    def parse_raw_def
      expect :KwRawDef
      id = expect(:Identifier, :Plus, :Minus, :Compare, :Slash, :Star)
      expect :BraceL

      arguments = [] of ArgumentAST
      if expect? :Pipe
        arguments << parse_argument
        while expect? :Comma
          arguments << parse_argument
        end
      end

      if expect? :Arrow
        ret_type = parse_type
      else
        ret_type = TypeAST.new("Nil")
      end

      strings = [] of StringLitAST
      while true
        strings << parse_quote
        break unless expect? :Comma
      end
      expect :BraceR

      return RawDefAST.new(id.str, arguments, ret_type, strings)
    end

    def parse_block(arguments? : Bool, ret_type? : Bool)
      expect :BraceL

      body = [] of ExpressionAST
      arguments = [] of ArgumentAST
      needs_semi = false

      if arguments? && expect? :Pipe
        arguments << parse_argument
        while expect? :Comma
          arguments << parse_argument
        end
        needs_semi = true
      end

      if ret_type? && expect? :Arrow
        ret_type = parse_type
        needs_semi = true
      else
        ret_type = TypeAST.new("Nil")
      end

      while true
        case
        when expect? :BraceR
          ret_expr = nil
          break
        when expect? :Semicolon
          needs_semi = false
        else
          expected "Expressions must be separated by ';' or '\n'" if needs_semi && !new_line?
          body << parse_expression
          needs_semi = true
        end
      end

      return {arguments, ret_type, body}
    end

    def parse_argument
      type = parse_type
      name = expect :Identifier

      return ArgumentAST.new(name.str, type)
    end

    def parse_type
      name = expect :Constant
      return TypeAST.new(name.str)
    end

    def parse_expression
      leftmost = parse_expression_no_binop

      if expect? :Assign
        unless leftmost.is_a? LVarAST
          expected "Left-hand side of assignment operator must be a local variable"
        end
        other = parse_expression
        return AssignmentAST.new(leftmost, other)
      end

      if expect? :Dot
        callee = expect(:Identifier).str

        arguments = [] of ExpressionAST
        unless new_line?
          if expect? :ParenL # Args are in parens
            while true
              arguments << parse_expression
              expect?(:ParenR) ? break : expect(:Comma)
            end
          elsif EXPR_BEGINNERS.includes?(peek) # Args are hangin' loose
            arguments << parse_expression
            while expect? :Comma
              arguments << parse_expression
            end
          end
        end

        return MethodAST.new(leftmost, callee, arguments)
      end

      # TODO make sure this precedence makes sense
      binary_precedence = [
        [ TTID::Star, TTID::Slash ],
        [ TTID::Plus, TTID::Minus ],
        [ TTID::Compare, TTID::Less, TTID::Greater, TTID::LessEq, TTID::GreaterEq ],
        [ TTID::And ],
        [ TTID::Or ],
      ]

      op_chain = [] of Token
      exprs = [ leftmost ] of ExpressionAST

      while binary_precedence.flatten.includes? peek
        op_chain << shift.not_nil!
        exprs << parse_expression_no_binop
      end

      binary_precedence.each do |ops|
        while idx = op_chain.index{ |op| ops.includes? op.type_id }
          op = op_chain.delete_at idx
          lhs = exprs.delete_at idx
          rhs = exprs[idx]
          exprs[idx] = MethodAST.new(lhs, op.str, [rhs])
        end
      end

      raise "Bug! Binary ops: #{op_chain}" unless op_chain.empty?
      raise "Bug! Expressions: #{exprs}" unless exprs.size == 1

      return exprs.first
    end

    EXPR_BEGINNERS = [TTID::Constant, TTID::Identifier, TTID::Int,
      TTID::QuoteL, TTID::ParenL, TTID::BraceL, TTID::KwRet]
    def parse_expression_no_binop
      case peek
      when TTID::Constant
        return parse_call
      when TTID::Identifier
        # If this is followed by an expression, it's a function call
        # Arg-less calls require parens to disambiguate from lvars.
        if EXPR_BEGINNERS.includes?(peek 1) && !new_line?(1)
          return parse_call
        else
          return parse_lvar
        end
      when TTID::Int
        return parse_int
      when TTID::QuoteL
        return parse_quote
      when TTID::ParenL
        shift
        return parse_expression.tap{ expect :ParenR }
      when TTID::BraceL
        return parse_lambda
      when TTID::KwRet
        return parse_ret
      else
        expected "Expression"
      end
    end

    def parse_call
      namespace = [] of String
      while name = expect? :Constant
        namespace << name.str
        expect :Dot
      end
      id = expect(:Identifier)

      arguments = [] of ExpressionAST
      unless new_line?
        if expect? :ParenL # Args are in parens
          while true
            arguments << parse_expression
            expect?(:ParenR) ? break : expect(:Comma)
          end
        elsif EXPR_BEGINNERS.includes?(peek) # Args are hangin' loose
          arguments << parse_expression
          while expect? :Comma
            arguments << parse_expression
          end
        end
      end

      return CallAST.new(id.str, namespace, arguments)
    end

    def parse_lvar
      id = expect :Identifier
      return LVarAST.new(id.str)
    end

    def parse_quote
      expect :QuoteL
      value = parse_str_body.bytes
      expect :QuoteR

      return StringLitAST.new(value)
    end

    def parse_str_body
      ret = String.build do |str|
        while true
          case peek
          when TTID::StringBody
            str << expect(:StringBody).str
          when TTID::EscapeByte
            case escape = expect(:EscapeByte).str
            when "\\n"
              str << '\n'
            when "\\\""
              str << '"'
            else
              error "Unrecognized escape: #{escape}"
            end
          when TTID::EscapeDecimal
            str << expect(:EscapeDecimal).str[1..-1].to_u8.chr
          when TTID::EscapeHex
            str << expect(:EscapeHex).str[2..-1].to_u8(16).chr
          else
            break
          end
        end
      end

      return ret
    end

    def parse_int
      int = expect :Int
      value = int.str.to_i64
      return IntLitAST.new(value)
    end

    def parse_lambda
      arguments, ret_type, body = parse_block(true, true)

      return LambdaLitAST.new(arguments, ret_type, body)
    end

    def parse_ret
      expect :KwRet
      return RetAST.new(parse_expression)
    end

    def expect?(*type_ids : TTID)
      tok = peek
      return shift.not_nil! if type_ids.includes? tok
      return nil
    end

    def expect(*type_ids : TTID)
      tok = shift
      return tok if tok && type_ids.includes? tok.type_id
      error("Expected #{type_ids}; got #{tok.try &.type_id}")
    end

    def expected(*args)
      tok = shift
      error "Expected #{args}; got #{tok.try &.type_id}"
    end

    def error(msg)
      raise ParseError.new(msg, shift)
    end
  end

  class ParseError < Exception
    @token : Token?
    def initialize(@message, @token)
    end

    def message
      if tok = @token
        "#{@message} at #{tok.str.inspect} (#{tok.row + 1}:#{tok.col + 1})"
      else
        "#{@message} at EOF"
      end
    end
  end
end
