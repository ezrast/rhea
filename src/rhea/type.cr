module Rhea
  module Type
    abstract def ir_repr : String
    abstract def name : String
    abstract def friendly : String

    def size
      raise "Size not implemented for #{self.class.name}"
    end
  end

  module ValueType
    include Type
  end

  # built-ins
  class BIValue
    include ValueType

    getter ir_repr : String
    getter friendly : String
    def initialize(@ir_repr, @friendly)
    end

    def name
      "%#{@friendly}"
    end
  end

  module BIString
    extend Type
    extend self

    def ir_repr
      "{ i64, i8* }" # Should be Word, not i64
    end

    def name
      "%.String*"
    end

    def friendly
      ".String"
    end
  end

  module BIBool
    extend ValueType
    extend self

    def ir_repr
      "i1"
    end

    def name
      "%.Bool"
    end

    def friendly
      ".Bool"
    end
  end

  module BILambda
    extend Type
    extend self

    def ir_repr
      "void()"
    end

    def name
      "%.Lambda*"
    end

    def friendly
      ".Lambda"
    end
  end

  module BINoReturn
    extend ValueType
    extend self

    def ir_repr
      raise "BINoReturn#ir_repr should not get called"
    end

    def name
      "void"
    end

    def friendly
      ".NoReturn"
    end
  end

  class Slice
    include Type

    @id : String
    def initialize(@id)
    end

    def ir_repr
      "{%.Word, i8*}"
    end

    def name
      "%.#{@id}*"
    end

    def friendly
      ".#{@id}"
    end

    def size
      2
    end
  end
end
