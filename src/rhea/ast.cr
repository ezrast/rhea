module Rhea
  abstract class ASTNode
  end

  class TopLevelAST < ASTNode
    getter imports : Array(ImportAST)
    getter actors : Array(ActorAST)
    getter slices : Array(SliceAST)
    getter raw_defs : Array(RawDefAST)
    getter bi_types : Array(BIValueAST)
    def initialize(@imports, @actors, @slices, @raw_defs, @bi_types)
    end
  end

  class ImportAST < ASTNode
    getter path : String
    getter type_names : Array(String)
    def initialize(@path, @type_names)
    end
  end

  class BIValueAST < ASTNode
    getter name : String
    getter ir_name : String
    getter raw_defs : Array(RawDefAST)
    def initialize(@name, @ir_name, @raw_defs)
    end
  end

  class ActorAST < ASTNode
    getter name : String
    getter states : Array(StateAST)
    getter defs : Array(DefAST)
    getter raw_defs : Array(RawDefAST)
    def initialize(@name, @states, @defs, @raw_defs)
    end
  end

  class SliceAST < ASTNode
    getter name : String
    getter inits : Array(InitAST)
    getter defs : Array(DefAST)
    getter fin : Array(ExpressionAST)
    def initialize(@name, @inits, @defs, @fin)
    end
  end

  class StateAST < ASTNode
    getter name : String
    getter arguments : Array(ArgumentAST)
    getter body : Array(ExpressionAST)
    def initialize(@name, @arguments, @body)
    end
  end

  class InitAST < ASTNode
    getter name : String
    getter arguments : Array(ArgumentAST)
    getter body : Array(ExpressionAST)
    def initialize(@name, @arguments, @body)
    end
  end

  class DefAST < ASTNode
    getter name : String
    getter arguments : Array(ArgumentAST)
    getter type : TypeAST
    getter body : Array(ExpressionAST)
    def initialize(@name, @arguments, @type, @body)
    end
  end

  class RawDefAST < ASTNode
    getter name : String
    getter arguments : Array(ArgumentAST)
    getter type : TypeAST
    getter strings : Array(StringLitAST)
    def initialize(@name, @arguments, @type, @strings)
    end
  end

  class ArgumentAST < ASTNode
    getter name : String
    getter type : TypeAST
    def initialize(@name, @type)
    end
  end

  class TypeAST < ASTNode
    getter name : String
    def initialize(@name)
    end
  end

  abstract class ExpressionAST < ASTNode
  end

  class CallAST < ExpressionAST
    getter callee : String
    getter namespace : Array(String)
    getter arguments : Array(ExpressionAST)
    def initialize(@callee, @namespace, @arguments)
    end

    def full_callee
      String.build{ |str| namespace.each{ |ns| str << '.' << ns }; str << '.' << callee }
    end
  end

  class MethodAST < ExpressionAST
    getter receiver : ExpressionAST
    getter callee : String
    getter arguments : Array(ExpressionAST)
    def initialize(@receiver, @callee, @arguments)
    end
  end

  class AssignmentAST < ExpressionAST
    getter lvar : LVarAST
    getter value : ExpressionAST
    def initialize(@lvar, @value)
    end
  end

  class LVarAST < ExpressionAST
    getter name : String
    def initialize(@name)
    end
  end

  abstract class LiteralAST < ExpressionAST
  end

  class StringLitAST < LiteralAST
    getter value : Array(UInt8)
    def initialize(@value)
    end
  end

  class IntLitAST < LiteralAST
    getter value : Int64
    def initialize(@value)
    end

    def ir_name
      value.to_s
    end
  end

  class LambdaLitAST < LiteralAST
    getter arguments : Array(ArgumentAST)
    getter ret_type : TypeAST
    getter body : Array(ExpressionAST)
    def initialize(@arguments, @ret_type, @body)
    end
  end

  class RetAST < ExpressionAST
    getter value : ExpressionAST
    def initialize(@value)
    end
  end
end
