require "./compilation_unit"
require "./type_map"
require "./function_signature"

module Rhea
  class Compiler
    @import_paths : Array(String)
    def initialize(source : String, @import_paths)
      @import_paths.map!{ |path| File.expand_path(path) + File::SEPARATOR }
      @compilation_units = {} of String => CompilationUnit

      core_source = File.read(find_full_import_path "_core")
      make_compilation_unit core_source, "_core"
      make_compilation_unit source, "_root"

      # TODO make sure imported names don't collide with local names
      @imported_function_signatures = {} of String => Hash(String, FunctionSignature)
      @full_function_signatures = {} of String => Hash(String, FunctionSignature)
      @full_type_maps = {} of String => TypeMap
      @lvar_types = {} of String => Hash(String, Hash(String, Type))

      # TODO refactor to be more efficient / less confusing
      @compilation_units.each do |id, cu|
        imported_function_signatures = {} of String => FunctionSignature
        imported_types = TypeMap.new
        imported_list = [] of {String, String}

        merge_imports(imported_list, "_core", "", id, imported_function_signatures, imported_types)
        cu.ast.imports.each do |import_ast|
          import_ast.type_names.each do |type_name|
            merge_imports(imported_list, import_ast.path, type_name, id, imported_function_signatures, imported_types)
          end
        end

        cu.typecheck(imported_function_signatures)
        unless (duplicate_functions = imported_function_signatures.keys & cu.function_signatures.keys).empty?
          raise "Functions defined twice: #{duplicate_functions}"
        end

        full_function_signatures = imported_function_signatures.merge cu.function_signatures
        full_type_map = imported_types.merge cu.type_map
        LOG.debug{ "Scanning lvar types..." }
        lvar_types = Scanner::LVarScanner.scan cu.ast, full_function_signatures, full_type_map

        @imported_function_signatures[id] = imported_function_signatures
        @full_function_signatures[id] = full_function_signatures
        @full_type_maps[id] = full_type_map
        @lvar_types[id] = lvar_types
      end
    end

    def compile_to(out_path : String)
      @compilation_units.each do |id, cu|
        LOG.debug{ " === Emitting compilation unit: #{id}" }
        out_file_name = File.join(out_path, id.gsub("/", ".") + ".ll")
        File.open(out_file_name, "w") do |file|
          Emitter.emit(
            file,
            cu.ast,
            @full_function_signatures[id],
            @lvar_types[id],
            cu.constants,
            @full_type_maps[id],
            @imported_function_signatures[id]
          )
        end
        puts out_file_name
      end
    end

    private def make_compilation_unit(source, name)
      @compilation_units[name]?.try{ |cu| return cu }
      LOG.debug{ " === Making compilation unit: #{name}" }

      cu = @compilation_units[name] = CompilationUnit.new(source)
      cu.ast.imports.each do |import_ast|
        imported_name = import_ast.path
        imported_full_path = find_full_import_path(imported_name)
        imported_source = File.read(imported_full_path)
        make_compilation_unit(imported_source, imported_name)
      end
    end

    private def find_full_import_path(rel_path)
      ret = @import_paths.each
        .map{ |bp| File.join(bp, rel_path + ".rh") }
        .select{ |full_path| File.exists? full_path }
        .first?
      raise "Could not find #{rel_path} in any of #{@import_paths}" unless ret
      return ret
    end

    private def merge_imports(imported_list, path, type_name, id, imported_function_signatures, imported_types)
      LOG.debug{ "Importing #{type_name} from #{path} to #{id}" }
      unless imported_list.includes?({path, type_name}) || path == id
        imported_list << {path, type_name}
        other_cu = @compilation_units[path]
        other_cu.function_signatures.each do |name, sig|
          next unless type_name.empty? || name.starts_with? "." + type_name + "."
          raise "Function defined twice" if imported_function_signatures.includes? name
          imported_function_signatures[name] = sig
        end
        other_cu.type_map.each do |name, type|
          next unless type_name.empty? || name.starts_with? type_name + "."
          imported_types[name] = type
        end
        other_cu.ast.imports.each do |import_ast|
          import_ast.type_names.each do |next_type_name|
            next unless next_type_name.empty? || next_type_name == type_name
            merge_imports(imported_list, import_ast.path, next_type_name, id, imported_function_signatures, imported_types)
          end
        end
      end
    end
  end
end
