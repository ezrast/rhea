require "./ast"

module Rhea
  # Traverses the AST and doesn't do anything with it
  # Subclasses can override individual methods
  abstract class Scanner
    macro inherited
      def self.scan(top_level, *args)
        new(*args).scan_top_level(top_level)
      end
    end

    @path = [] of String

    def path_str
      String.build{ |str| @path.each{ |id| str << '.' << id } }
    end

    def scan_top_level(node : TopLevelAST)
      node.actors.each{ |actor| @path << actor.name; scan_actor actor; @path.pop }
      node.slices.each{ |slice| @path << slice.name; scan_slice slice; @path.pop }
      node.raw_defs.each{ |de| @path << de.name; scan_raw_def de; @path.pop }
      node.bi_types.each{ |type| @path << type.name; scan_bi_type type; @path.pop }
    end

    def scan_bi_type(node : BIValueAST)
      node.raw_defs.each{ |de| @path << de.name; scan_raw_def de; @path.pop }
    end

    def scan_actor(node : ActorAST)
      node.states.each{ |state| @path << state.name; scan_state state; @path.pop }
      node.defs.each{ |de| @path << de.name; scan_def de; @path.pop }
      node.raw_defs.each{ |de| @path << de.name; scan_raw_def de; @path.pop }
    end

    def scan_slice(node : SliceAST)
      node.inits.each{ |init| @path << init.name; scan_init init; @path.pop }
      node.defs.each{ |de| @path << de.name; scan_def de; @path.pop }
      @path << "Fin"
      node.fin.each{ |expr| scan_expr expr }
      @path.pop
    end

    def scan_state(node : StateAST)
      node.body.each{ |expr| scan_expr expr }
    end

    def scan_init(node : InitAST)
      node.body.each{ |expr| scan_expr expr }
    end

    def scan_def(node : DefAST)
      node.body.each{ |expr| scan_expr expr }
    end

    def scan_raw_def(node : RawDefAST)
    end

    def scan_argument(node : ArgumentAST)
    end

    def scan_type(node : TypeAST)
    end

    def scan_expr(node : CallAST)
      node.arguments.each { |arg| scan_expr arg }
    end

    def scan_expr(node : MethodAST)
      scan_expr node.receiver
      node.arguments.each { |arg| scan_expr arg }
    end

    def scan_expr(node : AssignmentAST)
      scan_expr node.lvar
      scan_expr node.value
    end

    def scan_expr(node : LVarAST)
    end

    def scan_expr(node : LambdaLitAST)
      node.body.each{ |expr| scan_expr expr }
    end

    def scan_expr(node : LiteralAST)
    end

    def scan_expr(node : RetAST)
      scan_expr node.value
    end
  end
end
