require "./type"

module Rhea
  class FunctionSignature
    getter argument_types : Array(String)
    getter ret_type : String
    def initialize(@argument_types, @ret_type)
    end
  end
end
