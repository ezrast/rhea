require "./token"
require "./token_type"

module Rhea
  class Lexer
    def self.lex(source : String)
      Lexer.new(source).lex
    end

    @lines : Array(String)
    @comment = String::Builder.new
    protected def initialize(source)
      @lines = source.lines(false)
      @col = 0
      @row = 0
      @new_line = true

      @tokens = [] of Token
    end

    def lex
      mode_stack = [TokenType::Context::Main]

      while true
        mode = mode_stack.last
        if @row >= @lines.size
          return @tokens
        end

        next_mode = lex_mode(TokenType::CONTEXTS[mode])

        if next_mode
          if next_mode == TokenType::Context::Pop
            mode_stack.pop
          else
            mode_stack << next_mode
          end
        end
      end
    end

    def lex_mode(mode)
      mode.each do |token_type|
        if match = @lines[@row][@col..-1].match token_type.regex
          str = match[0]
          unless (type_id = token_type.type_id).none?
            if type_id.comment?
              if @new_line
                @comment << str[1..-1] << '\n'
              end
            else
              comment = unless @comment.empty?
                @comment.to_s.tap{ @comment = String::Builder.new }
              end
              @tokens << Token.new(type_id, str, @row, @col, @new_line, comment)
              @new_line = false
            end
          end
          @col += str.size
          if @col >= @lines[@row].size
            @new_line = true
            @row += 1
            @col = 0
          end

          return token_type.mode
        end
      end

      msg = "Lexer error at #{@row+1}:#{@col+1}\n#{@lines[@row].chomp}\n#{" " * (@col)}^\n"
      raise msg
    end
  end
end
