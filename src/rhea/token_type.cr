module Rhea
  class TokenType
    getter regex : Regex
    getter type_id : TokenTypeID
    getter mode : Context?
    protected def initialize(regex, @type_id : TokenTypeID, @mode = nil)
      @regex = /^#{regex}/
    end

    enum Context
      Main
      StringLiteral
      MainBrace
      MainParen
      MainBracket
      Pop
    end

    MAIN_CONTEXT = [
      new(/[0-9]+\.[0-9]+(e[0-9]+)?+/, :Float),
      new(/[0-9]+/, :Int),

      new(/&&/, :And),
      new(/\|\|/, :Or),
      new(/>=/, :GreaterEq),
      new(/<=/, :LessEq),
      new(/;/, :Semicolon),
      new(/:=/, :Assign),
      new(/:/, :Colon),
      new(/->/, :Arrow),
      new(/\!/, :Bang),
      new(/\|/, :Pipe),
      new(/\?/, :Question),
      new(/,/, :Comma),
      new(/\./, :Dot),
      new(/\*/, :Star),
      new(/\+/, :Plus),
      new(/-/, :Minus),
      new(/\//, :Slash),
      new(/>/, :Greater),
      new(/</, :Less),
      new(/==/, :Compare),
      new(/=>/, :Rocket),
      #/# Fix broken syntax highlighting

      new(/"/, :QuoteL, Context::StringLiteral),
      new(/\{/, :BraceL, Context::MainBrace),
      new(/\(/, :ParenL, Context::MainParen),
      new(/@\[/, :SliceL, Context::MainBracket),
      new(/\[/, :BracketL, Context::MainBracket),

      new(/Import/, :KwImport),
      new(/CoreImport/, :KwCoreImport),
      new(/BIValue/, :KwBiValue),
      new(/IR/, :KwIr),
      new(/Actor/, :KwActor),
      new(/State/, :KwState),
      new(/Slice/, :KwSlice),
      new(/Derive/, :KwDerive),
      new(/Init/, :KwInit),
      new(/Def/, :KwDef),
      new(/RawDef/, :KwRawDef),
      new(/Fin/, :KwFin),
      new(/Ret/, :KwRet),
      new(/End/, :KwEnd),

      new(/@[a-z][a-zA-Z0-9_]*/, :Ivar),
      new(/[a-z][a-zA-Z0-9_]*/, :Identifier),
      new(/[A-Z][a-zA-Z0-9_]*/, :Constant),

      new(/#.*/, :Comment),
      new(/\s+/, :None)
    ]

    CONTEXTS = {
      Context::Main => MAIN_CONTEXT,

      Context::MainBrace => [
        new(/\}/, :BraceR, Context::Pop)
      ] + MAIN_CONTEXT,

      Context::MainParen => [
        new(/\)/, :ParenR, Context::Pop)
      ] + MAIN_CONTEXT,

      Context::MainBracket => [
        new(/\]/, :BracketR, Context::Pop)
      ] + MAIN_CONTEXT,

      Context::StringLiteral => [
        new(/"/, :QuoteR, Context::Pop),
        new(/\\\d+/, :EscapeDecimal),
        new(/\\x[a-fA-F0-9]{2}/, :EscapeHex),
        new(/\\{/, :InterpL, Context::MainBrace),
        new(/[^\\"]+/, :StringBody),
        new(/\\./, :EscapeByte),
      ]
    }

    enum TokenTypeID
      Float; Int;
      And; Or;
      GreaterEq; LessEq;
      Semicolon;
      Colon; Arrow; Bang;
      Assign; Pipe; Question;
      Comma; Dot;
      Star; Plus; Minus; Slash;
      Greater; Less; Compare;
      Rocket;

      QuoteL; BraceL; ParenL; SliceL; BracketL;
      QuoteR; BraceR; ParenR; SliceR; BracketR;

      EscapeDecimal; EscapeHex; EscapeByte;
      InterpL; StringBody;

      KwImport; KwCoreImport;
      KwBiValue; KwIR;
      KwActor; KwState; KwSlice;
      KwDerive; KwInit; KwDef; KwRawDef; KwFin;
      KwRet;
      KwEnd;
      Ivar;

      Identifier;
      Constant;

      Comment;
      None;
    end
  end
end
