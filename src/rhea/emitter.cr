require "./scanner/constant_scanner"

module Rhea
  class Emitter
    struct Nested
      getter type : Type
      getter id : String
      def initialize(@type, @id)
      end

      def to_s(io)
        io << @type.name << ' ' << @id
      end
    end

    def self.emit(*args)
      new(*args).emit_top_level
    end

    @expr_id = 0

    @io : IO
    @top_level : TopLevelAST
    @function_types : Hash(String, FunctionSignature)
    @imported_function_types : Hash(String, FunctionSignature)
    @lvar_types : Hash(String, Hash(String, Type))
    @constants : Scanner::ConstantScanner::Constants
    @type_map : TypeMap
    private def initialize(@io, @top_level, @function_types, @lvar_types, @constants, @type_map, @imported_function_types)
      @indent = 0
      @new_line = false
      @path = [] of String
    end

    def path_str
      String.build{ |str| @path.each{ |id| str << '.' << id } }
    end

    def indent
      @indent += 1
    end

    def dedent
      @indent -= 1
    end

    def emit_indent
      @indent.times{ |ind| @io << "  " } if @new_line
      @new_line = false
    end

    def emit(*args : String | Char | Int32 | Nested)
      emit_indent
      args.each{ |arg| @io << arg }
    end

    def emit_byte(byte)
      emit_indent
      if (32..126).includes? byte
        @io.write_byte byte
      else
        @io << '\\' << byte.to_s(16).rjust(2, '0')
      end
    end

    def line(*args : String | Char | Int32 | Nested)
      emit_indent
      emit *args, '\n'
      @new_line = true
    end

    def line
      emit '\n'
    end

    ################

    def emit_top_level
      @type_map.each{ |name, type| emit '%', name, " = type ", type.ir_repr, '\n' }

      if @top_level.actors.find{ |actor| actor.name == "Main" }
        emit <<-IR
        define i32 @_start() {
          call i32 @main()
          call %.Word @.Sys.exit(%.Word 0)
          unreachable
        }

        define i32 @main() {
          call void @.Main.init()
          ret i32 0
        }


        IR
      end

      @imported_function_types.each do |func_name, signature|
        ret_type = @type_map[@path, signature.ret_type]
        emit "declare ", ret_type.name, " @\"", func_name, "\"("
        signature.argument_types.each_with_index do |type_name, idx|
          type = @type_map[@path, type_name]
          emit ", " if idx > 0
          emit type.name
        end
        line ")"
      end

      @constants.strings.each do |value, idx|
        emit "@STR_#{idx}.inner = private constant [#{value.size} x i8] c\""
        value.each{ |byte| emit_byte byte} # todo escape special chars
        line '"'
        line "@STR_#{idx} = private constant %.String { %.Word #{value.size}, i8* getelementptr([#{value.size} x i8], [#{value.size} x i8]* @STR_#{idx}.inner, i64 0, i64 0) }"
      end

      @constants.lambdas.each do |lambda, idx|
        line "define void @LAMBDA_", idx, "() {"
        indent
        lambda.body.each{ |expr| emit_expr expr }
        dedent
        line "ret void"
        line '}'
      end

      @top_level.bi_types.each{ |type| emit_bi_type type }
      @top_level.actors.each{ |actor| emit_actor actor }
      @top_level.slices.each{ |slice| emit_slice slice }
      @top_level.raw_defs.each{ |de| emit_raw_def de  }
    end

    def emit_slice(node : SliceAST)
      @path << node.name
      node.inits.each{ |init| emit_init init }
      node.defs.each{ |de| emit_def de  }

      # emit_fin node.fin
      @path.pop
    end

    def emit_bi_type(node : BIValueAST)
      @path << node.name
      node.raw_defs.each{ |de| emit_raw_def de }
      @path.pop
    end

    def emit_actor(node : ActorAST)
      @path << node.name
      node.states.each{ |state| emit_state state }
      node.defs.each{ |de| emit_def de  }
      node.raw_defs.each{ |de| emit_raw_def de  }
      @path.pop
    end

    def emit_state(node : StateAST)
      @path << node.name

      emit "define void @#{path_str}("
      node.arguments.each_with_index do |arg, idx|
        emit ", " unless idx == 0
        emit "%$#{idx}"
      end
      line ") {"
      indent
      @lvar_types[path_str].each do |name, type|
        line '%', name, " = alloca ", type.name
      end
      node.arguments.each_with_index do |arg, idx|
        line "store ", @lvar_types[path_str][arg.name].name, " %$", idx, ", ", @lvar_types[path_str][arg.name].name, "* %", arg.name
      end

      node.body.each{ |expr| emit_expr expr }
      line "ret void"
      dedent
      line "}"
      line

      @path.pop
    end

    def emit_init(node : InitAST)
      type = @type_map[@path]
      @path << node.name

      emit "define #{type.name} @#{path_str}("
      node.arguments.each_with_index do |arg, idx|
        emit ", " unless idx == 0
        emit_type arg.type
        emit " %$#{idx}"
      end
      line ") {"
      indent
      @lvar_types[path_str].each do |name, type|
        line '%', name, " = alloca ", type.name
      end
      node.arguments.each_with_index do |arg, idx|
        line "store ", @lvar_types[path_str][arg.name].name, " %$", idx, ", ", @lvar_types[path_str][arg.name].name, "* %", arg.name
      end

      line "%_new_slice_word = call %.Word @.Sys.mmap(%.Word ", type.size, ')'
      line "%_new_slice = inttoptr %.Word %_new_slice_word to ", type.name

      node.body.each{ |expr| emit_expr expr }
      line "ret #{type.name} %_new_slice"

      dedent
      line "}"
      line

      @path.pop
    end

    def emit_def(node : DefAST)
      @path << node.name

      emit "define #{@type_map[@path, node.type].name} @#{path_str}("
      node.arguments.each_with_index do |arg, idx|
        emit ", " unless idx == 0
        emit_type arg.type
        emit " %$#{idx}"
      end
      line ") {"
      indent
      @lvar_types[path_str].each do |name, type|
        line '%', name, " = alloca ", type.name
      end
      node.arguments.each_with_index do |arg, idx|
        line "store ", @lvar_types[path_str][arg.name].name, " %$", idx, ", ", @lvar_types[path_str][arg.name].name, "* %", arg.name
      end

      node.body.each{ |expr| emit_expr expr }
      unless node.body.find{ |expr| expr.is_a? RetAST }
        line "ret %.Nil {}"
      end

      dedent
      line "}"
      line

      @path.pop
    end

    def emit_raw_def(node : RawDefAST)
      @path << node.name

      emit "define #{@type_map[@path, node.type].name} @\"#{path_str}\"("
      node.arguments.each_with_index do |arg, idx|
        emit ", " unless idx == 0
        emit_type arg.type
        emit " %#{arg.name}"
      end
      line ") {"
      indent

      node.strings.each{ |str| str.value.each{ |byte| emit_byte byte}; line }

      dedent
      line "}"
      line

      @path.pop
    end

    def emit_argument(node : ArgumentAST)
      emit_type node.type
      emit " %", node.name
    end

    def emit_type(node : TypeAST)
      emit @type_map[@path, node].name
    end

    # emit_expr methods return a structure whose to_s is like
    # "%some_type %.123" that outer expressions use to reference inner
    # expressions (LLVM IR doesn't allow nesting in most contexts).
    def emit_expr(node : CallAST) : Nested
      nested_args = node.arguments.map{ |arg| emit_expr arg }
      expr_id = (@expr_id += 1)

      type = @type_map[@path, @function_types[node.full_callee].ret_type]
      emit "%.#{expr_id} = call #{type.name} @\""
      node.namespace.each{ |ns| emit '.', ns }
      emit '.', node.callee, "\"("

      nested_args.each_with_index do |arg, arg_idx|
        emit ", " if arg_idx > 0
        emit arg
      end
      line ")"

      return Nested.new(type, "%.#{expr_id}")
    end

    def emit_expr(node : MethodAST) : Nested
      receiver = emit_expr node.receiver
      nested_args = node.arguments.map{ |arg| emit_expr arg }
      expr_id = (@expr_id += 1)

      full_callee = "#{receiver.type.friendly}.#{node.callee}"
      type = @type_map[@path, @function_types[full_callee].ret_type]
      emit "%.", expr_id, " = call ", type.name, " @\"", full_callee, "\"("

      emit receiver
      nested_args.each do |arg|
        emit ", ", arg
      end
      line ")"

      return Nested.new(type, "%.#{expr_id}")
    end

    def emit_expr(node : AssignmentAST) : Nested
      rhs = emit_expr node.value
      line "store ", rhs, ", ", @lvar_types[path_str][node.lvar.name].name, "* %", node.lvar.name
      return rhs
    end

    def emit_expr(node : LVarAST) : Nested
      expr_id = (@expr_id += 1)
      type = @lvar_types[path_str][node.name]
      line "%.", expr_id, " = load ", type.name, ", ", type.name, "* %", node.name

      return Nested.new(type, "%.#{expr_id}")
    end

    # Literals are no-ops by themselves, so they don't usually emit
    # anything, but still need to return a value that other expressions
    # can use.
    def emit_expr(node : StringLitAST) : Nested
      expr_id = (@expr_id += 1)
      idx = @constants.strings[node.value]
      line "%.", expr_id, " = load %.String, %.String* @STR_#{idx}"
      return Nested.new(@type_map[".String"], "%.#{expr_id}")
    end

    def emit_expr(node : IntLitAST) : Nested
       return Nested.new(@type_map[".Word"], node.value.to_s)
    end

    def emit_expr(node : LambdaLitAST) : Nested
      @path << node.object_id.to_s
      idx = @constants.lambdas[node]
      @path.pop
      return Nested.new(BILambda, "@LAMBDA_#{idx}")
    end

    def emit_expr(node : RetAST) : Nested
      line "ret ", emit_expr node.value
      return Nested.new(BINoReturn, "Error: Don't nest return expressions")
    end
  end
end
