module Rhea
  class Token
    getter type_id : TokenType::TokenTypeID
    getter str : String
    getter row : Int32
    getter col : Int32
    getter? new_line : Bool
    getter comment : String?
    def initialize(@type_id, @str, @row, @col, @new_line, @comment)
    end
  end
end
