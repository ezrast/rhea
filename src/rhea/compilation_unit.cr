require "./lexer"
require "./parser"
require "./scanner/*"
require "./emitter"
require "./type"
require "./type_map"

module Rhea
  class CompilationUnit
    @source : String
    @tokens : Array(Token)
    getter ast : TopLevelAST
    getter type_map : TypeMap
    getter function_signatures : Hash(String, FunctionSignature)
    getter constants : Scanner::ConstantScanner::Constants

    def initialize(@source)
      ### Lex and parse
      LOG.debug{ "Lexing..." }
      @tokens = Lexer.lex @source
      LOG.debug{ "Found #{@tokens.size} tokens. Parsing..." }
      @ast = Parser.parse @tokens
      LOG.debug{ "Parsing complete. Scanning function types..." }

      ### Get locally-defined types
      @type_map = Scanner::TypeScanner.scan @ast
      LOG.debug{ "Found types: #{@type_map.each.to_a}" }

      ### Scan local function types
      @function_signatures = Scanner::FunctionScanner.scan @ast, @type_map
      LOG.debug{ "Found #{@function_signatures.size} functions." }

      @constants = Scanner::ConstantScanner.scan @ast
      LOG.debug{ "Found #{@constants.size} constants." }
    end

    def typecheck(imported_function_signatures : Hash(String, FunctionSignature))
      Scanner::TypeChecker.scan @ast, @function_signatures.merge(imported_function_signatures)
    end
  end
end
