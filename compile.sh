set -e
dir=`dirname $0`
ll_files=`RHEA_IMPORT_PATH="$dir/stdlib" ./bin/rhea ir "$1"`

function build {
  for ll in $ll_files; do
    llc -filetype=obj "$ll" -o "$ll.o"
    echo "built $ll.o" > /dev/stderr
    echo "$ll.o"
  done
}
ld $ll -o "$1.out" `build`
